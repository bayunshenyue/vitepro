export interface IMenubarList {
  name: string;
  path: string;
  meta: {
    icon?: string;
    title: string;
    hidden?: boolean; // 是否隐藏路由
    alwaysShow?: boolean; // 当子路由只有一个的时候是否显示当前路由
  };
  component?: any;
  children?: IMenubarList[];
}
