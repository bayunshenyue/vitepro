export interface ITabs {
  title: string;
  path: string;
}
