import { ITabs } from '@/type/router/loyout';

export interface State {
  collapsed: boolean;
  tableList: ITabs[];
}
