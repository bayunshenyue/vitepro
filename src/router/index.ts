import type { RouteLocationNormalized, RouteRecordRaw } from 'vue-router';
import { createRouter, createWebHistory } from 'vue-router';

export const routes: any[] = [];

export const modules = import.meta.globEager('./modules/*.ts');
for (const path in modules) {
  routes.push(...modules[path].default);
}

const route: any = createRouter({
  history: createWebHistory(),
  routes: routes,
});

// to: 即将要进入的目标let data = Mock.mock([
// from: 当前导航正要离开的路由
//false: 取消当前的导航
//一个路由地址:通过一个路由地址跳转到一个不同的地址router.push() 一样
route.beforeResolve((to: RouteLocationNormalized, from: RouteLocationNormalized) => {
  return;
});

export default route;
