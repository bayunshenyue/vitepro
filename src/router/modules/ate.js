import { Layout } from '@/router/constant';
const routes = [
    {
        name: 'home',
        path: '/',
        component: Layout,
        meta: {
            icon: 'akar-icons:location',
            title: '首页',
            hidden: false,
        },
        children: [],
    },
];
export default routes;
//# sourceMappingURL=ate.js.map