import { IMenubarList } from '@/type/router/router';

const routes: IMenubarList[] = [
  {
    name: 'test',
    path: '/test',
    component: () => import('@/components/MeBu.vue'),
    meta: {
      icon: 'subway:brightest',
      title: 'test',
    },
  },
];

export default routes;
