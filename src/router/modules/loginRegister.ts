import { IMenubarList } from '@/type/router/router';

const routes: IMenubarList[] = [
  {
    name: 'login',
    path: '/login',
    component: () => import('@/views/LoginRegister.vue'),
    meta: {
      icon: 'ri:login-circle-fill',
      title: '登录',
      hidden: false,
    },
    children: [],
  },
];

export default routes;
