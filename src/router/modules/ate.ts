import { IMenubarList } from '@/type/router/router';
import { Layout } from '@/router/constant';

const routes: IMenubarList[] = [
  {
    name: 'home',
    path: '/',
    component: Layout,
    meta: {
      icon: 'akar-icons:location',
      title: '首页',
      hidden: false,
    },
    children: [],
  },
];

export default routes;
