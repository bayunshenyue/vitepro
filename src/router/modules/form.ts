import { IMenubarList } from '@/type/router/router';
import { Layout } from '@/router/constant';

const routes: IMenubarList[] = [
  {
    name: 'Form',
    path: '/form',
    component: Layout,
    meta: {
      icon: 'clarity:form-line',
      title: '表单',
    },
    children: [
      {
        path: '/form/basic-form',
        name: 'form-basic-form',
        meta: {
          title: '基础表单',
          icon: 'grommet-icons:form-schedule',
        },
        component: () => import('@/views/form/basicForm.vue'),
      },
      {
        path: '/form/step-form',
        name: 'form-step-form',
        meta: {
          title: '分步表单',
          icon: 'ic:baseline-dynamic-form',
        },
        component: () => import('@/views/form/stepForm.vue'),
      },
      {
        path: 'detail',
        name: 'form-detail',
        meta: {
          title: '表单详情',
          icon: 'icon-park-outline:form',
        },
        component: () => import('@/views/form/detail.vue'),
      },
    ],
  },
];

export default routes;
