const routes = [
    {
        name: 'login',
        path: '/login',
        component: () => import('@/views/LoginRegister.vue'),
        meta: {
            icon: 'ri:login-circle-fill',
            title: '登录',
            hidden: false,
        },
        children: [],
    },
];
export default routes;
//# sourceMappingURL=loginRegister.js.map