const routes = [
    {
        name: '404',
        path: '/:catchAll(.*)',
        component: () => import('@/views/404.vue'),
        meta: {
            icon: 'codicon:error',
            title: '404',
        },
    },
];
export default routes;
//# sourceMappingURL=error.js.map