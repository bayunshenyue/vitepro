import { IMenubarList } from '@/type/router/router';

const routes: IMenubarList[] = [
  {
    name: '404',
    path: '/:catchAll(.*)',
    component: () => import('@/views/404.vue'),
    meta: {
      icon: 'codicon:error',
      title: '404',
    },
  },
];

export default routes;
