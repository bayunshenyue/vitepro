import zh_TW from '@/locales/zh-TW';
import zh_CN from '@/locales/language/zh-CN';
import ja from '@/locales/language/ja';
import en from '@/locales/language/en';
// 语言库
export const messages = {
    zh_CN: zh_CN,
    zh_TW: zh_TW,
    en: en,
    ja: ja,
};
//# sourceMappingURL=getLangs.js.map