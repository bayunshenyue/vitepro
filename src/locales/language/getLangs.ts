import zh_TW from '@/locales/language/zh-TW';
import zh_CN from '@/locales/language/zh-CN';
import ja from '@/locales/language/ja';
import en from '@/locales/language/en';
import zh_CNLocale from 'element-plus/lib/locale/lang/zh-cn'
import enLocale from 'element-plus/lib/locale/lang/en'
import jaLocale from 'element-plus/lib/locale/lang/ja'
import zh_twLocale from 'element-plus/lib/locale/lang/zh-tw'

// 语言库
export const messages = {
  zh_CN: {...zh_CN,...zh_CNLocale},
  zh_TW: {...zh_TW,...zh_twLocale},
  en: {...en,...enLocale},
  ja: {...ja,...jaLocale},
};
