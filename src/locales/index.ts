import { createI18n } from 'vue-i18n';
import { messages } from './language/getLangs';
import zh_CN from "@/locales/language/zh-CN";

const i18n = createI18n({
  fallbackLocale:'zh_CN',
  globalInjection:true,
  locale:'zh_CN',
  messages,
});

export default i18n;
