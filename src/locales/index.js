import { createI18n } from 'vue-i18n';
import { messages } from './language/getLangs';
const i18n = createI18n({
    legacy: false,
    locale: window.localStorage.getItem('lang') || 'zh_CN',
    messages,
});
export default i18n;
//# sourceMappingURL=index.js.map