import { mock, Random } from 'mockjs';
Random.extend({
    tag: function () {
        const tag = ['家', '公司', '学校', '超市'];
        return this.pick(tag);
    }
});
const tableList = mock({
    // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
    'list|100': [{
            // 属性 id 是一个自增数，起始值为 1，每次增 1
            'id|+1': 1,
            date: () => Random.date('yyyy-MM-dd'),
            name: () => Random.name(),
            address: () => Random.cparagraph(1),
            tag: () => Random.tag(),
            amt: () => Number(Random.float(-100000, 100000).toFixed(2))
        }]
});
const responseData = (code, msg, data) => {
    return {
        Code: code,
        Msg: msg,
        Data: data
    };
};
export default [
    {
        url: '/api/User',
        method: 'get',
        timeout: 300,
        response: (req) => {
            return responseData(200, '登录成功', req);
        }
    },
];
//# sourceMappingURL=index.js.map