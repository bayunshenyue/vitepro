import { table } from './data/user';
import { MockMethod } from 'vite-plugin-mock';
import Mcok from 'mockjs';

const Rondom = Mcok.Random;
export default [
  {
    url: '/api/uer',
    method: 'get',
    response: () => {
      return {
        code: 0,
        message: '调用数据成功',
        data: Mcok.mock({
          'list|30': [
            {
              username: '@name',
              password: '@string',
              email: '@email',
            },
          ],
        }),
      };
    },
  },
];

//   {
//     path: '@string',
//     component: '@string',
//     alwaysShow: 'true',
//     meta: {
//       icon: 'akar-icons:location',
//       title: '首页',
//     },
//     'children|3': [
//       {
//         path: '@string',
//         component: '@string',
//         alwaysShow: 'true',
//         meta: {
//           icon: 'gg:menu-round',
//           title: "导航@cword('零一二三四五六七八九十', 1)",
//         },
//       },
//     ],
//   },
// ]);
//
// const sj = Mock.mock({
//   'list|20': [
//     {
//       path: '@string',
//       component: '@string',
//       alwaysShow: 'true',
//       meta: {
//         icon: 'gg:menu-round',
//         title: "导航@cword('一二三四五六七八九十', 1)",
//       },
//     },
//   ],
// });
// const we = () => {
//   sj.list.forEach((node: any) => {
//     data.push(node);
//   });
// };
// we();
