import { createProdMockServer } from "vite-plugin-mock/es/createProdMockServer";
import testMock from "@/mock";
export const mockModules = [...testMock];
export function setupProdMockServer() {
    createProdMockServer(mockModules);
}
//# sourceMappingURL=mockProdServer.js.map