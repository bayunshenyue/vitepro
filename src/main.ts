import { createApp } from 'vue';
import App from './App.vue';
// 路由
import router from './router';
// vuex
import { createPinia } from 'pinia';
// 全局样式
import '@/assets/tailwind.css';
//icon
import '@purge-icons/generated';
import 'virtual:svg-icons-register';
//国际化
import i18n from '@/locales';

const app = createApp(App);
const pinia = createPinia();
app.use(i18n)
app.use(router);
app.use(pinia);
app.mount('#app');
