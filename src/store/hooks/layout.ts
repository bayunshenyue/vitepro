import { defineStore } from 'pinia';
import { State } from '@/type/store';
import { ITabs } from '@/type/router/loyout';

const useLayoutStore = defineStore({
  id: 'layout',
  state: (): State => ({
    collapsed: false,
    tableList: [],
  }),
  getters: {
    getCollapsed:(state:State):boolean=>{
      return state.collapsed;
    },
    getTabs: (state: State): ITabs[] => {
      return state.tableList;
    },
  },
  actions: {
    changeCollapsed(): void {
      this.collapsed = !this.collapsed;
    },
    addTabe(tab: ITabs): void {
      if (this.tableList.some(item => item.path === tab.path)) {
        return;
      }
      this.tableList.push(tab);
    },
  },
});

export default useLayoutStore;
