import { defineStore } from 'pinia';
const useCounterStore = defineStore({
    id: 'count',
    // 数据源
    state: () => ({ count: 1 }),
    // 计算属性
    getters: {
        doubleCount: state => state.count * 2,
        doublePlusOne(state) {
            return (n) => state.count * 2;
        },
    },
    // 异步方法
    actions: {
        increment() {
            this.count++;
        },
        remen() {
            localStorage.removeItem('count-store');
        },
        async addAsyncWith() {
            let any = await anyAwit();
        },
    },
});
const anyAwit = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(30);
        }, 1000);
    });
};
const instance = useCounterStore();
// 保存
instance.$subscribe((mutation, state) => {
    localStorage.setItem('count-store', JSON.stringify({ ...state }));
});
// 获取
const old = localStorage.getItem('count-store');
if (old) {
    instance.$patch({
        ...JSON.parse(old),
    });
}
export default useCounterStore;
//# sourceMappingURL=useCounterStore.js.map