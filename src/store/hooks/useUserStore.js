import { defineStore } from 'pinia';
export const KEY_USER_ID = 'user';
const useUserStore = defineStore({
    id: KEY_USER_ID,
    state: () => ({
        id: -1,
        username: '',
        role: '',
        email: '',
        token: '',
    }),
    actions: {
        setToken(token) {
            this.$state.token = token;
        },
        setId(id) {
            this.$state.id = id;
        },
        setRole(role) {
            this.$state.role = role;
        },
        //Partial将参数变可选
        login(user) {
            this.$state = {
                ...user,
                ...this.$state,
            };
        },
    },
});
const instance = useUserStore();
instance.$subscribe((mutation, state) => {
    localStorage.setItem(instance.$id, JSON.stringify(state));
});
//初始化
const val = localStorage.getItem(instance.$id);
if (val) {
    instance.$patch({
        ...JSON.parse(val),
    });
}
export default useUserStore;
//# sourceMappingURL=useUserStore.js.map