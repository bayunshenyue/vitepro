import { defineStore } from 'pinia';
const useLayoutStore = defineStore({
    id: 'layout',
    state: () => ({
        collapsed: false,
        tableList: [],
    }),
    getters: {
        getTabs: (state) => {
            return state.tableList;
        },
    },
    actions: {
        changeCollapsed() {
            this.collapsed = !this.collapsed;
        },
        addTabe(tab) {
            if (this.tableList.some(item => item.path === tab.path)) {
                return;
            }
            this.tableList.push(tab);
        },
    },
});
export default useLayoutStore;
//# sourceMappingURL=layout.js.map