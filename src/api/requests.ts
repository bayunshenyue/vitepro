import axios, { AxiosRequestConfig, Method } from "axios";
import { KEY_USER_ID, UserInfo } from "@/store/hooks/useUserStore";
import { NO_PERMISSION, OK_CODE } from "@/app/keys";
import router from "@/router";
import { ElMessage } from "element-plus";

// 创建 axios 实例
const requests = axios.create({
  // 服务接口请求
  baseURL: import.meta.env.VITE_APP_BASE_API as string,
  // 超时设置
  timeout: 15000,
  withCredentials: true,
  headers: { "Content-Type": "application/json;charset=UTF-8" }
});

// 请求拦截器
requests.interceptors.request.use(
  // 登录流程控制中，根据本地是否存在token判断用户的登录情况
  // 但是即使token存在，也有可能token是过期的，所以在每次的请求头中携带token
  // 后台根据携带的token判断用户的登录情况，并返回给我们对应的状态码
  // 而后我们可以在响应拦截器中，根据状态码进行一些统一的操作。
  config => {
    config = config || {};
    try {
      const user = JSON.parse(localStorage.getItem(KEY_USER_ID) || "") as UserInfo;
      if (user.token) {
        config.headers!["Authorization"] = `Bearer ${user.token}`;
      }
    } catch (e) {
    }
    return config;
  },
  error => {
    Promise.reject(error)
      .then(() => ElMessage.error(
        {
          message: JSON.stringify(error),
          showClose: true,
          center: true
        }
      ));
  }
);

// 响应拦截器
requests.interceptors.response.use(
  response => {
    const {
      code,
      msg
    } = response.data || {};

    if (response.status !== OK_CODE) {
      Promise.reject(msg)
        .then();
    }
    if (code === NO_PERMISSION) {
      router.push({ name: "Login" });
      return Promise.reject(msg);
    }
    return Promise.resolve(response);
  },
  error => {
    const { response } = error;
    if (response) {
      ElMessage.error({
        showClose: true,
        message: JSON.stringify(error),
        center: true
      });
      return Promise.reject(response);
    }
    return Promise.reject(error);
  }
);
export default requests;
