import type { AxiosRequestConfig } from 'axios';

export interface BasicRes<T> {
  code: number;
  data: T;
  msg: string;
}

export interface RequestInterfaces {
  requestInter?: (config: AxiosRequestConfig) => AxiosRequestConfig;
}
