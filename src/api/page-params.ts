// 全局统一分页参数类型声明
import { usePagination } from 'vue-request';
import axios from 'axios';

declare interface PageParams {
  current: number; //当前页数
  page: number; //总页数
  total: number; //数据条数
  type?: Model; // 可选参数
  readonly sort?: string; // 只读可选参数
}

interface Model {
  type?: string;
}

export default PageParams;
