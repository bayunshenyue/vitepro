import useHttp from '@/api/useHttp';
import { BasicRes } from '@/api/types';

export interface RegParams {
  username: string;
  password: string;
  email: string;
}

export const reqUserRegister = (params: RegParams) => {
  return useHttp<BasicRes<RegParams>>({
    url: `/use`,
    method: 'get',
  });
};
