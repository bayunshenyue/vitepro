// // 拦截器
// import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
// import { KEY_USER_ID, UserInfo } from '@/store/hooks/useUserStore';
// import { ElMessage } from 'element-plus';
// import { NO_PERMISSION, OK_CODE } from '@/app/keys';
// import router from '@/router';
//
// export class Interceptors {
//   public instance: any;
//
//   constructor() {
//     // 创建axios实例
//     this.instance = axios.create({ timeout: 1000 * 12 });
//     // 初始化拦截器
//     this.initInterceptors();
//   }
//
//   // 为了让http.ts中获取初始化好的axios实例
//   public getInterceptors() {
//     return this.instance;
//   }
//
//   // 初始化拦截器
//   public initInterceptors() {
//     instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
//     /**
//      * 请求拦截器
//      * 每次请求前，如果存在token则在请求头中携带token
//      */
//     this.instance.interceptors.request.use(
//       (config: AxiosRequestConfig) => {
//         // 登录流程控制中，根据本地是否存在token判断用户的登录情况
//         // 但是即使token存在，也有可能token是过期的，所以在每次的请求头中携带token
//         // 后台根据携带的token判断用户的登录情况，并返回给我们对应的状态码
//         try {
//           const user = JSON.parse(localStorage.getItem(KEY_USER_ID) || '') as UserInfo;
//           if (user.token) {
//             config.headers!['Authorization'] = `Bearer ${user.token}`;
//           }
//         } catch (e) {}
//         return config;
//       },
//       (error: any) => {
//         ElMessage.error(error);
//       }
//     );
//     // 响应拦截器
//     this.instance.interceptors.response.use(
//       (res: AxiosResponse) => {
//         const { code, msg } = res.data || {};
//         if (code !== OK_CODE) {
//           return ElMessage.success(msg);
//         }
//         if (code === NO_PERMISSION) {
//           router.push({ name: 'login' });
//           return ElMessage.success(msg);
//         }
//         return Promise.resolve(res);
//       },
//       (error: any) => {
//         return ElMessage.error(error);
//       }
//     );
//   }
// }
