import requests from '@/api/requests';
const useHttp = (config) => {
    return new Promise((resolve, reject) => {
        requests({
            url: config.url,
            method: config.method,
            data: config.data || {},
            params: config.params || {},
        })
            .then(res => {
            resolve(res.data);
        })
            .catch(err => {
            reject(err);
        });
    });
};
export default useHttp;
//# sourceMappingURL=useHttp.js.map