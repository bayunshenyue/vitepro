import { defineConfig,loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';
import { viteMockServe } from 'vite-plugin-mock';
import PurgeIcons from 'vite-plugin-purge-icons';
import AutoImport from 'unplugin-auto-import/vite';
import viteSvgIcons from 'vite-plugin-svg-icons';
import { AntDesignVueResolver, NaiveUiResolver, ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import path from 'path';


const resolve = (dir: string) => path.join(__dirname, dir);
export default defineConfig({
  plugins: [
    vue(),
    viteSvgIcons({
      iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
      // Specify symbolId format
      symbolId: 'icon-[dir]-[name]',
    }),
    viteMockServe({
      mockPath: '@/mock',
      watchFiles: true,
    }),
    Components({
      resolvers: [AntDesignVueResolver(), NaiveUiResolver(), ElementPlusResolver()],
      extensions: ['vue'],
      dts: 'src/components.d.ts',
      directoryAsNamespace: true,
    }),
    AutoImport({
      imports: ['vue'],
    }),
    PurgeIcons({
      /* 清除图标选项 */
      content: [
        '**/*.html',
        '**/*.js',
        '**/*.vue', // scan for .vue file as well
      ],
    }),
  ],
  resolve: {
    alias: {
      //find是别名，replacement是要替换的路径
      '@': resolve('src'),
      comps: resolve('src/components'),
      apis: resolve('src/api'),
      views: resolve('src/views'),
      utils: resolve('src/utils'),
      store: resolve('src/store'),
      routes: resolve('src/routes'),
      layouts: resolve('src/layouts'),
      assets: resolve('src/assets'),
      hooks: resolve('src/hooks'),
    },
  },
  server: {
    port: 8080, //启动端口
    open: true,
  },
});
