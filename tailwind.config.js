module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false,
  theme: {
    extend: {
      // 颜色
      colors: {
        rose: {
          50: '#fff1f2',
          100: '#ffe4e6',
          200: '#fecdd3',
          300: '#fda4af',
          400: '#fb7185',
          500: '#f43f5e',
          600: '#e11d48',
          700: '#be123c',
          800: '#9f1239',
          900: '#881337',
        },
        fuchsia: {
          50: '#fdf4ff',
          100: '#fae8ff',
          200: '#f5d0fe',
          300: '#f0abfc',
          400: '#e879f9',
          500: '#d946ef',
          600: '#c026d3',
          700: '#a21caf',
          800: '#86198f',
          900: '#701a75',
        },
        indigo: {
          50: '#eef2ff',
          100: '#e0e7ff',
          200: '#c7d2fe',
          300: '#a5b4fc',
          400: '#818cf8',
          500: '#6366f1',
          600: '#4f46e5',
          700: '#4338ca',
          800: '#3730a3',
          900: '#312e81',
        },
        // indigo: {
        //   50:'',
        //   100:'',
        //   200:'',
        //   300:'',
        //   400:'',
        //   500:'',
        //   600:'',
        //   700:'',
        //   800:'',
        //   900:''
        // },
        cyan: {
          50: '#ecfeff',
          100: '#cffafe',
          200: '#a5f3fc',
          300: '#67e8f9',
          400: '#22d3ee',
          500: '#06b6d4',
          600: '#0891b2',
          700: '#0e7490',
          800: '#155e75',
          900: '#164e63',
        },
        lime: {
          50: '#f7fee7',
          100: '#ecfccb',
          200: '#d9f99d',
          300: '#bef264',
          400: '#a3e635',
          500: '#84cc16',
          600: '#65a30d',
          700: '#4d7c0f',
          800: '#3f6212',
          900: '#365314',
        },
        el: '#409eff',
        light: '#85d7ff',
        anyuzi: '#5c2223',
        mudafh: '#eea2a4',
        lizi: '#5a191b',
        xeh: '#f07c82',
        ptjz: '#5a1216',
        yh: '#ed5a65',
        yuh: '#c04851',
        ygb: '#e2e1e4',
        snh: '#2f2f35',
        dlz: '#a7a8bd',
        qfl: '#2c9678',
        xyh: '#617172',
        qh: '#c4d7d6',
        shl: '#1a3b32',
        yyb: '#c0c4c3',
        wzl: '#69a794',
        gll: '#1f2623',
        fql: '#497568',
        qsl: '#93d5dc',
        htl: '#c6e6e8',
        hyl: '#40a070',
        wtl: '#69a794',
        yb: '#eef7f2',
        ql: '#3c9566',
        yzl: '#a4cab6',
        gdl: '#20894d',
        dcl: '#c6dfc8',
        tnl: '#c6dfc8',
        fsh: '#fed71a',
        sjh: '#b78b26',
        ezh: '#fbb929',
        yyh: '#685e48',
        myth: '#f9d27d',
        yhs: '#80766e',
        jz: '#806d9e',
        al: '#4973ff',
        ss: '#fffbf0',
        cs: '#75878a',
        qhs: '#161823',
        chl: '#9ed900',
        hl: '#30dff3',
        ch: '#395260',
        xf: '#ed5736',
      },
      // 间距
      spacing: {
        1: '1px',
        0.75: '0.75rem',
        0.93: '0.9375rem',
        32: '2rem',
        7.5: '7.5rem', //120px
        2.5: '2.5rem', //40px
        3.125: '3.125rem', //50px
        1.25: '1.25rem',
        4.06: '4.06rem',
        2.82: '2.82rem',
        6.88: '6.88rem',
        12.5: '12.5rem', //200px
        18: '18rem',
        44: '44%',
        '11/12': '91.666667%',
        '1/12': '8.333333%',
        '10/12': '83.333333%',
        '9/12': '75%',
        '8/12': '66.666667%',
        '7/12': '58.333333%',
        '2/12': '16.666667%',
        double: '200%',
      },
      // 最大宽度
      maxWidth: {
        12.5: '12.5rem', //200px
      },
      // 最小宽度
      minWidth: {},
      // 最小高度
      minHeight: {
        search: '100%',
      },
      // 边框半径
      borderRadius: {
        2.5: '2.5rem',
        '2/5': '40%',
        45: '45%',
      },
      // 字体大小
      fontSize: {
        1.25: '1.25rem',
        32: '2rem',
        '10x1': ['10rem', 1],
      },
      // 字母间距
      letterSpacing: {
        0.5: '0.5rem',
      },
      // 字体高度
      lineHeight: {
        50: '3.125rem',
      },
      // Z-Index 范围
      zIndex: {
        1: 1,
      },
      // 定位Top / Right / Bottom / Left scale
      inset: {
        '-58': '-5rem',
      },
      // 阴影
      boxShadow: {
        '5x1': 'inset 0 0 50px rgba(0, 0, 0, .5)',
      },
      // 动画
      keyframes: {
        wave: {
          '0%': { transform: 'translate(-50%, -75%) rotate(0deg)' },
          '100%': {
            transform: 'translate(-50%, -75%) rotate(360deg)',
          },
        },
      },
      animation: {
        wave: 'wave 10s linear infinite',
      },
      // 过渡
      transitionProperty: {},
      // 字体样式
      fontFamily: {
        current:
          'SimSun, SimHei, Microsoft YaHei,  Microsoft JhengHei, NSimSun, PMingLiU, MingLiU, DFKai-SB, FangSong,KaiTi,STHeiti Light [STXihei],STHeiti,STSong ,STFangsong,LiHei Pro Medium,LiSong Pro Light,BiauKai,Apple LiGothic Medium',
      },
      // 响应前缀
      screens: {},
    },
  },
  // 变体
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/forms')],
};
